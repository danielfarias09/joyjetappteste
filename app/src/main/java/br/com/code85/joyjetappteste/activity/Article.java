package br.com.code85.joyjetappteste.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.DefaultSliderView;

import java.sql.SQLException;

import br.com.code85.joyjetappteste.R;
import br.com.code85.joyjetappteste.dao.DatabaseHelper;
import br.com.code85.joyjetappteste.dao.FavoriteDao;
import br.com.code85.joyjetappteste.model.Favorite;
import br.com.code85.joyjetappteste.model.Item;

public class Article extends AppCompatActivity {
    private Item item;
    private SliderLayout sliderLayout;
    private TextView tvTitle;
    private TextView tvCategory;
    private TextView tvDescription;
    private String category;
    private DatabaseHelper dh;
    private FavoriteDao dao;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_article);

        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();
        category = bundle.getString("category");

        item = (Item) bundle.getSerializable("items");

        sliderLayout = (SliderLayout)findViewById(R.id.slider);
        tvTitle = (TextView)findViewById(R.id.title);
        tvCategory = (TextView)findViewById(R.id.category);
        tvDescription = (TextView)findViewById(R.id.description);

        //Aqui serão atribuídas as imagens de cada item para o imageSlider
        for(String photo: item.getGalery()){
            DefaultSliderView slider = new DefaultSliderView(this);
            slider.image(photo);
            sliderLayout.addSlider(slider);
        }

        tvTitle.setText(item.getTitle());
        tvCategory.setText(category);
        tvDescription.setText(item.getDescription());

        //Abre a conexão com o banco de dados
        dh = new DatabaseHelper(this);
        try {
            dao = new FavoriteDao(dh.getConnectionSource());
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public void back(View view){
        finish();
    }

    public void addFavorite(View view){
        try {
            Favorite favorite = new Favorite();
            favorite.setCategory("Category " + category);
            favorite.setTitle(item.getTitle());
            dao.create(favorite);
            Toast.makeText(this,R.string.success_favorite ,Toast.LENGTH_LONG).show();
        } catch (SQLException e) {
            e.printStackTrace();
            Toast.makeText(this,R.string.error_favorite ,Toast.LENGTH_LONG).show();
        }

    }

    //Fecha a conexão com o banco
    @Override
    public void onDestroy(){
        super.onDestroy();
        dh.close();

    }

}
