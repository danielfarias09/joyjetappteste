package br.com.code85.joyjetappteste.dao;

import com.j256.ormlite.dao.BaseDaoImpl;
import com.j256.ormlite.support.ConnectionSource;

import java.sql.SQLException;

import br.com.code85.joyjetappteste.model.Favorite;

/**
 * Created by Daniel on 04/02/2017.
 */

//Primeiro argumento é a classe e o segundo é o tipo da primary key
public class FavoriteDao extends BaseDaoImpl<Favorite, Integer> {

    public FavoriteDao(ConnectionSource cs) throws SQLException {
        super(Favorite.class);
        setConnectionSource(cs);
        initialize();
    }
}
