package br.com.code85.joyjetappteste.task;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.List;

import br.com.code85.joyjetappteste.activity.Article;
import br.com.code85.joyjetappteste.adapter.ItemAdapter;
import br.com.code85.joyjetappteste.model.Card;
import br.com.code85.joyjetappteste.model.Item;
import br.com.code85.joyjetappteste.utils.HttpHandler;
import br.com.code85.joyjetappteste.utils.RecyclerItemClickListener;

/**
 * Created by Daniel on 01/02/2017.
 */

public class TaskChooseCategory extends AsyncTask <String,String,List<Item>> {
    private RecyclerView recyclerView;
    private static String url = "https://cdn.joyjet.com/tech-interview/mobile-test-one.json";
    private Context context;
    private String category;


    public TaskChooseCategory(Context context, RecyclerView recyclerView, String category){
        this.context = context;
        this.recyclerView = recyclerView;
        this.category = category;
    }

    @Override
    protected List<Item> doInBackground(String... strings) {
        HttpHandler handler = new HttpHandler();
        List<Card> listCards;
        List<Item> listItems = null;

        //Recebe o Json
        String jsonString = handler.callService(url);

        if(jsonString != null){
            Gson gson = new Gson();

            Type collectionType = new TypeToken<List<Card>>(){}.getType();

            listCards = gson.fromJson(jsonString, collectionType);

            //Envia os itens de acordo com a categoria
            for(Card card: listCards){
                if(card.getCategory().equals(category)){
                    listItems =  card.getItems();
                }
            }

            return  listItems;
        } else {

            Toast.makeText(context, "Empty List", Toast.LENGTH_SHORT).show();
            return null;
        }
    }

    @Override
    protected void onPostExecute(final List<Item> listItems){
        ItemAdapter adapter = new ItemAdapter(listItems,context);
        recyclerView.setAdapter(adapter);
        recyclerView.addOnItemTouchListener(new RecyclerItemClickListener(context, new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {

                Intent intent = new Intent(context, Article.class);
                Bundle bundle = new Bundle();
                bundle.putSerializable("items", listItems.get(position));
                bundle.putString("category",category);
                intent.putExtras(bundle);
                context.startActivity(intent);

            }
        }));

    }
}
