package br.com.code85.joyjetappteste.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.mikepenz.materialdrawer.AccountHeader;
import com.mikepenz.materialdrawer.AccountHeaderBuilder;
import com.mikepenz.materialdrawer.Drawer;
import com.mikepenz.materialdrawer.DrawerBuilder;
import com.mikepenz.materialdrawer.model.PrimaryDrawerItem;
import com.mikepenz.materialdrawer.model.ProfileDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IDrawerItem;

import br.com.code85.joyjetappteste.R;
import br.com.code85.joyjetappteste.task.TaskShowCategories;

public class ListCategory extends AppCompatActivity {
    private RecyclerView recyclerView;
    private Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_category);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        initializeMaterialDrawer();

        recyclerView = (RecyclerView)findViewById(R.id.recycleView);
        recyclerView.setHasFixedSize(true);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));
        recyclerView.setLayoutManager(layoutManager);

        TaskShowCategories tsc = new TaskShowCategories(this, recyclerView);
        tsc.execute();
    }

    private void initializeMaterialDrawer(){
        // Criando o AccountHeader
        AccountHeader headerResult = new AccountHeaderBuilder()
                .withActivity(this)
                .withHeaderBackground(R.color.colorAccountHeader)
                .addProfiles(
                        new ProfileDrawerItem().withIcon(getResources().getDrawable(R.drawable.ic_joyjet))
                )
                .build();

        PrimaryDrawerItem item1 = new PrimaryDrawerItem().withName(R.string.home);
        PrimaryDrawerItem item2 = new PrimaryDrawerItem().withName(R.string.favorites);

        //Criando o navigation drawer e passando o AccountHeader Result
        new DrawerBuilder()
                .withAccountHeader(headerResult)
                .withActivity(this)
                .withToolbar(toolbar)
                .addDrawerItems(
                        item1, item2
                )
                .withOnDrawerItemClickListener(new Drawer.OnDrawerItemClickListener() {
                    @Override
                    public boolean onItemClick(View view, int position, IDrawerItem drawerItem) {

                        switch (position){
                            case 1:
                                callHome();
                                break;
                            case 2:
                                callFavorites();
                                break;
                        }

                        return false;

                    }
                })
                .build();
    }


    private void callHome(){
        Intent intent = new Intent(this,Menu.class);
        startActivity(intent);
    }

    private void callFavorites(){
        Intent intent = new Intent(this,Favorites.class);
        startActivity(intent);
    }


}
