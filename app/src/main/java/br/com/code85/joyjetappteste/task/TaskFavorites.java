package br.com.code85.joyjetappteste.task;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import br.com.code85.joyjetappteste.activity.Article;
import br.com.code85.joyjetappteste.adapter.ItemAdapter;
import br.com.code85.joyjetappteste.model.Card;
import br.com.code85.joyjetappteste.model.Favorite;
import br.com.code85.joyjetappteste.model.Item;
import br.com.code85.joyjetappteste.utils.HttpHandler;
import br.com.code85.joyjetappteste.utils.RecyclerItemClickListener;

/**
 * Created by Daniel on 04/02/2017.
 */

public class TaskFavorites extends AsyncTask<String,String,List<Item>> {

    private RecyclerView recyclerView;
    private static String url = "https://cdn.joyjet.com/tech-interview/mobile-test-one.json";
    private Context context;
    private List<Favorite> favorites;

    public TaskFavorites(Context context, RecyclerView recyclerView, List<Favorite> favorites){
        this.context = context;
        this.recyclerView = recyclerView;
        this.favorites = favorites;
    }

    @Override
    protected List<Item> doInBackground(String... strings) {
        HttpHandler handler = new HttpHandler();
        List<Card> listCards = null;

        //Recebe o Json
        String jsonString = handler.callService(url);

        if (jsonString != null) {
            Gson gson = new Gson();

            Type collectionType = new TypeToken<List<Card>>() {
            }.getType();

            listCards = gson.fromJson(jsonString, collectionType);

            List<Item> itemsFavorites = new ArrayList<>();

            //Aqui estão sendo comparados os favoritos que estão salvos no banco com os favoritos do json
            //primeiro comparamos pela categoria e depois pelo título do item
            //Se existir algum favorito com essas características, ele é adicionado à lista dos favoritos e apresentado
            for(Favorite favorite: favorites){
                for(Card card: listCards){
                    if(favorite.getCategory().equals(card.getCategory())){
                        for(Item item: card.getItems()){
                            if(favorite.getTitle().equals(item.getTitle())){
                                itemsFavorites.add(item);
                            }
                        }
                    }
                }
            }

            return itemsFavorites;

        } else{

            Toast.makeText(context, "Empty List", Toast.LENGTH_SHORT).show();
            return null;
        }
    }

        protected void onPostExecute(final List<Item> itemsFavorites){


        ItemAdapter adapter = new ItemAdapter(itemsFavorites,context);
        recyclerView.setAdapter(adapter);

            recyclerView.addOnItemTouchListener(new RecyclerItemClickListener(context, new RecyclerItemClickListener.OnItemClickListener() {
                @Override
                public void onItemClick(View view, int position) {

                    Intent intent = new Intent(context, Article.class);
                    Bundle bundle = new Bundle();
                    bundle.putSerializable("items", itemsFavorites.get(position));
                    bundle.putString("category",favorites.get(position).getCategory());
                    intent.putExtras(bundle);
                    context.startActivity(intent);

                }
            }));


    }

}
