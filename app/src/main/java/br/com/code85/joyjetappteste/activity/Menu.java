package br.com.code85.joyjetappteste.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;

import br.com.code85.joyjetappteste.R;

public class Menu extends AppCompatActivity {
    private Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
    }

    public void openListFavorite(View view){
        Intent intent = new Intent(this,Favorites.class);
        startActivity(intent);
    }

    public void openListCategory(View view){
        Intent intent = new Intent(this,ListCategory.class);
        startActivity(intent);
    }


}
