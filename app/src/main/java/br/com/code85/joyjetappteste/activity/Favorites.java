package br.com.code85.joyjetappteste.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;

import java.sql.SQLException;
import java.util.List;

import br.com.code85.joyjetappteste.R;
import br.com.code85.joyjetappteste.dao.DatabaseHelper;
import br.com.code85.joyjetappteste.dao.FavoriteDao;
import br.com.code85.joyjetappteste.model.Favorite;
import br.com.code85.joyjetappteste.task.TaskFavorites;

public class Favorites extends AppCompatActivity {
    private RecyclerView recyclerView;
    private List<Favorite> favorites;
    private DatabaseHelper dh;
    private FavoriteDao dao;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_favorites);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        dh = new DatabaseHelper(this);
        try {
            dao = new FavoriteDao(dh.getConnectionSource());
            favorites = dao.queryForAll();
        } catch (SQLException e) {
            e.printStackTrace();
        }


        recyclerView = (RecyclerView)findViewById(R.id.recycleView);
        recyclerView.setHasFixedSize(true);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(layoutManager);

        TaskFavorites tf = new TaskFavorites(this,recyclerView,favorites);
        tf.execute();
    }

    //Fecha a conexão com o banco
    @Override
    public void onDestroy(){
        super.onDestroy();
        dh.close();

    }

}
