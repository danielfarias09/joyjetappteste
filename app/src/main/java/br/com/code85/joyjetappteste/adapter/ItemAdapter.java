package br.com.code85.joyjetappteste.adapter;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.DefaultSliderView;

import java.util.List;

import br.com.code85.joyjetappteste.R;
import br.com.code85.joyjetappteste.model.Item;

/**
 * Created by Daniel on 02/02/2017.
 */

public class ItemAdapter extends RecyclerView.Adapter<ItemAdapter.ItemViewHolder>{
    private List<Item> listItems;
    private Context context;

    public ItemAdapter(List<Item> listItems, Context context){
        this.listItems = listItems;
        this.context = context;
    }

    @Override
    public ItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_card, parent, false);
        ItemViewHolder viewHolder = new ItemViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ItemViewHolder holder, int position) {
        Item item = listItems.get(position);
        holder.tvTitle.setText(item.getTitle());
        holder.tvDescription.setText(item.getDescription());

        //Aqui serão atribuídas as imagens de cada item para o imageSlider
        for(String photo: item.getGalery()){
            DefaultSliderView slider = new DefaultSliderView(context);
            slider.image(photo);
            holder.slide.addSlider(slider);
        }

    }

    @Override
    public int getItemCount() {
        return listItems.size();
    }



    public static class ItemViewHolder extends RecyclerView.ViewHolder {
        CardView cardView;
        TextView tvTitle;
        TextView tvDescription;
        SliderLayout slide;

        public ItemViewHolder(View itemView) {
            super(itemView);

            cardView = (CardView)itemView.findViewById(R.id.cardView);
            tvTitle = (TextView)itemView.findViewById(R.id.title);
            tvDescription = (TextView)itemView.findViewById(R.id.description);
            slide = (SliderLayout)itemView.findViewById(R.id.slider);
        }
    }
}
