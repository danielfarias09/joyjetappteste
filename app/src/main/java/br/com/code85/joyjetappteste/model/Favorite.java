package br.com.code85.joyjetappteste.model;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * Created by Daniel on 04/02/2017.
 */

//Classe responsável por salvar os favoritos no banco de dados
@DatabaseTable(tableName="favorite")
public class Favorite{

    @DatabaseField(generatedId = true)
    private Integer id;

    @DatabaseField
    private String category;

    @DatabaseField
    private String title;

    public Favorite(){

    }

    public Favorite(String category, String title) {
        this.category = category;
        this.title = title;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
