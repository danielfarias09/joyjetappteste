package br.com.code85.joyjetappteste.task;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import br.com.code85.joyjetappteste.R;
import br.com.code85.joyjetappteste.activity.Categories;
import br.com.code85.joyjetappteste.adapter.CategoryAdapter;
import br.com.code85.joyjetappteste.model.Card;
import br.com.code85.joyjetappteste.utils.HttpHandler;
import br.com.code85.joyjetappteste.utils.RecyclerItemClickListener;

/**
 * Created by Daniel on 05/02/2017.
 */

public class TaskShowCategories extends AsyncTask<String,String,List<String>>{

    private RecyclerView recyclerView;
    private static String url = "https://cdn.joyjet.com/tech-interview/mobile-test-one.json";
    private Context context;

    public TaskShowCategories(Context context, RecyclerView recyclerView){
        this.context = context;
        this.recyclerView = recyclerView;
    }


    @Override
    protected List<String> doInBackground(String... strings) {
        HttpHandler handler = new HttpHandler();
        List<Card> listCards = null;
        List<String> listCategories = new ArrayList<>();

        //Recebe o Json
        String jsonString = handler.callService(url);

        if (jsonString != null) {
            Gson gson = new Gson();

            Type collectionType = new TypeToken<List<Card>>() {
            }.getType();

            listCards = gson.fromJson(jsonString, collectionType);

            for(Card card: listCards){
                listCategories.add(card.getCategory());
            }

            return listCategories;

        } else{

            Toast.makeText(context, R.string.empty, Toast.LENGTH_SHORT).show();
            return null;
        }
    }

    @Override
    protected void onPostExecute(final List<String> listCategories){
        CategoryAdapter adapter = new CategoryAdapter(context,listCategories);
        recyclerView.setAdapter(adapter);
        recyclerView.addOnItemTouchListener(new RecyclerItemClickListener(context, new RecyclerItemClickListener.OnItemClickListener() {

            @Override
            public void onItemClick(View view, int position) {
                Intent intent = new Intent(context, Categories.class);
                intent.putExtra("category",listCategories.get(position));
                context.startActivity(intent);

            }
        }));

    }
}
