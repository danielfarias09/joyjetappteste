package br.com.code85.joyjetappteste.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Daniel on 01/02/2017.
 */

public class Card {
    @SerializedName("category")
    @Expose
    private String category;
    @SerializedName("items")
    @Expose
    private List<Item> items = null;

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public List<Item> getItems() {
        return items;
    }

    public void setItems(List<Item> items) {
        this.items = items;
    }

}
