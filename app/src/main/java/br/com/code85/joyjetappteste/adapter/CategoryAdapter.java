package br.com.code85.joyjetappteste.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import br.com.code85.joyjetappteste.R;

/**
 * Created by Daniel on 05/02/2017.
 */

public class CategoryAdapter extends RecyclerView.Adapter<CategoryAdapter.CategoryViewHolder>{
    private List<String> listCategories;
    private Context context;

    public CategoryAdapter(Context context, List<String> listCategories){
        this.listCategories = listCategories;
        this.context = context;
    }


    @Override
    public CategoryViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_category, parent, false);
        CategoryAdapter.CategoryViewHolder viewHolder = new CategoryAdapter.CategoryViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(CategoryViewHolder holder, int position) {
        String category = listCategories.get(position);
        holder.tvCategory.setText(category);

    }

    @Override
    public int getItemCount() {
        return listCategories.size();
    }

    public static class CategoryViewHolder extends RecyclerView.ViewHolder {
        TextView tvCategory;

        public CategoryViewHolder(View itemView) {
            super(itemView);
            tvCategory = (TextView)itemView.findViewById(R.id.category);
        }
    }
}
