package br.com.code85.joyjetappteste.dao;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;

import java.sql.SQLException;

import br.com.code85.joyjetappteste.model.Favorite;

/**
 * Created by Daniel on 04/02/2017.
 */

public class DatabaseHelper extends OrmLiteSqliteOpenHelper {private static final String databaseName = "database.db";
    private static final int databaseVersion = 1;

    public DatabaseHelper(Context context) {
        super(context, databaseName, null, databaseVersion);
    }

    //Cria as tabelas no banco
    @Override
    public void onCreate(SQLiteDatabase sd, ConnectionSource cs) {
        try {
            TableUtils.createTable(cs, Favorite.class);
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onUpgrade(SQLiteDatabase sd, ConnectionSource cs, int oldVersion, int newVersion) {
        try {
            TableUtils.dropTable(cs, Favorite.class,true);
            onCreate(sd,cs);
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void close(){
        super.close();
    }

}
